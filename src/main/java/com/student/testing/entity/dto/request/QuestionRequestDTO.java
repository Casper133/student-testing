package com.student.testing.entity.dto.request;

public class QuestionRequestDTO {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
