package com.student.testing.entity.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AnswerRequestDTO {

    private String text;

    @JsonProperty("question_id")
    private Long questionId;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

}
