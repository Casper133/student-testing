package com.student.testing.entity.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeneralResponseDTO<T> {

    private int status;

    @JsonProperty("error_message")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorMessage;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T response;

    public GeneralResponseDTO(int status, String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public GeneralResponseDTO(T response) {
        status = 200;
        this.response = response;
    }

    public int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public T getResponse() {
        return response;
    }

}
