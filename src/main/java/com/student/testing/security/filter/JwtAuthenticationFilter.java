package com.student.testing.security.filter;

import com.student.testing.config.property.JwtProperties;
import com.student.testing.security.TokenType;
import com.student.testing.security.TokenUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authManager;

    private final JwtProperties jwtProperties;

    public JwtAuthenticationFilter(AuthenticationManager authManager, JwtProperties jwtProperties) {
        this.authManager = authManager;
        this.jwtProperties = jwtProperties;

        setFilterProcessesUrl(jwtProperties.getUrl());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(email, password);

        return authManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) {

        UserDetails userDetails = (UserDetails) authResult.getPrincipal();

        List<String> roles = userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        String accessToken = TokenUtils.createToken(jwtProperties, userDetails.getUsername(), roles,
                TokenType.ACCESS_TOKEN.toString(), jwtProperties.getAccessTokenExpirationTime());

        String refreshToken = TokenUtils.createToken(jwtProperties, userDetails.getUsername(), roles,
                TokenType.REFRESH_TOKEN.toString(), jwtProperties.getRefreshTokenExpirationTime());

        response.addHeader(jwtProperties.getAccessTokenHeader(), jwtProperties.getTokenPrefix() + accessToken);
        response.addHeader(jwtProperties.getRefreshTokenHeader(), jwtProperties.getTokenPrefix() + refreshToken);
    }

}
