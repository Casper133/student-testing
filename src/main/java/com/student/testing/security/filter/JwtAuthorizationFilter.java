package com.student.testing.security.filter;

import com.student.testing.config.property.JwtProperties;
import com.student.testing.security.TokenInfo;
import com.student.testing.security.TokenType;
import com.student.testing.security.TokenUtils;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtProperties jwtProperties;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, JwtProperties jwtProperties) {
        super(authenticationManager);
        this.jwtProperties = jwtProperties;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        Authentication authentication;

        try {
            authentication = getAuthentication(request, jwtProperties.getAccessTokenHeader(), TokenType.ACCESS_TOKEN);
        } catch (ExpiredJwtException e) {
            authentication = getAuthenticationByRefreshToken(request);
        }

        if (authentication == null) {
            chain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private Authentication getAuthenticationByRefreshToken(HttpServletRequest request) {
        try {
            return getAuthentication(request,
                    jwtProperties.getRefreshTokenHeader(), TokenType.REFRESH_TOKEN);
        } catch (ExpiredJwtException e) {
            return null;
        }
    }

    private Authentication getAuthentication(HttpServletRequest request, String tokenHeader, TokenType tokenType) {
        TokenInfo tokenInfo = TokenUtils
                .parseToken(request.getHeader(tokenHeader), jwtProperties);

        boolean tokenTypeNotCorrect = Objects.isNull(tokenInfo) ||
                !tokenInfo.getTokenType().equals(tokenType.toString());

        if (tokenTypeNotCorrect) {
            return null;
        }

        String email = tokenInfo.getEmail();

        if (StringUtils.isEmpty(email)) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(email, null, tokenInfo.getRoles());
    }

}
