package com.student.testing.security;

public enum TokenType {

    ACCESS_TOKEN,
    REFRESH_TOKEN

}
