package com.student.testing.security;

import com.student.testing.config.property.JwtProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

public final class TokenUtils {

    private TokenUtils() {
    }

    public static String createToken(JwtProperties jwtProperties, String email,
                                     List<String> roles, String tokenType, String expirationTime) {

        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(jwtProperties.getKey().getBytes()), SignatureAlgorithm.HS512)
                .setIssuer(jwtProperties.getTokenIssuer())
                .setAudience(jwtProperties.getTokenAudience())
                .setSubject(email)
                .setExpiration(Date.from(
                        LocalDateTime.now()
                                .plusMinutes(
                                        Long.parseLong(expirationTime)
                                ).atZone(ZoneId.systemDefault()).toInstant()))
                .claim("roles", roles)
                .claim("type", tokenType)
                .compact();
    }

    public static TokenInfo parseToken(String rawToken, JwtProperties jwtProperties) {
        if (rawToken == null || rawToken.length() == 0 || !rawToken.startsWith(jwtProperties.getTokenPrefix())) {
            return null;
        }

        byte[] signingKey = jwtProperties.getKey().getBytes();

        Jws<Claims> parsedToken = Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(rawToken.replace(jwtProperties.getTokenPrefix(), ""));

        if (parsedToken == null) {
            return null;
        }

        return new TokenInfo(parsedToken);
    }

}
