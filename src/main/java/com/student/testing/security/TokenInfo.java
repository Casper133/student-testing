package com.student.testing.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class TokenInfo {

    private String tokenType;

    private String email;

    private List<GrantedAuthority> roles;

    TokenInfo(Jws<Claims> parsedToken) {
        this.tokenType = (String) parsedToken.getBody().get("type");
        this.email = parsedToken.getBody().getSubject();
        this.roles = ((List<?>) parsedToken.getBody().get("roles"))
                .stream()
                .map(authority -> new SimpleGrantedAuthority((String) authority))
                .collect(Collectors.toList());
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<GrantedAuthority> getRoles() {
        return roles;
    }

    public void setRoles(List<GrantedAuthority> roles) {
        this.roles = roles;
    }

}
