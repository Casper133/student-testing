package com.student.testing.aspect;

import com.student.testing.entity.Question;
import com.student.testing.entity.User;
import com.student.testing.service.StudentTestCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Aspect
@Component
public class MethodsAspect {

    private Logger log = LogManager.getLogger(MethodsAspect.class);

    @Pointcut("within(com.student.testing..*)")
    public void appMethod() {
    }

    @Pointcut("execution(* com.student.testing.service.QuestionService.findAll())")
    public void readQuestionsMethod() {
    }

    @Pointcut("execution(* com.student.testing.service.UserService.findAll())")
    public void readUsersMethod() {
    }

    @Around("appMethod()")
    public Object logAllMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getName();

        log.info("Call method " + methodName + " with args " + Arrays.toString(joinPoint.getArgs()));

        Object returnValue = joinPoint.proceed();
        log.info("Finish method " + methodName);

        return returnValue;
    }

    @Around("readQuestionsMethod()")
    public Object cacheQuestions(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!StudentTestCache.questions.isEmpty()) {
            log.info("Return cached questions");
            return StudentTestCache.questions;
        }

        log.info("Return non-cached questions");
        Object methodResult = joinPoint.proceed();
        StudentTestCache.questions = (List<Question>) methodResult;
        return methodResult;
    }

    @Around("readUsersMethod()")
    public Object cacheUsers(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!StudentTestCache.users.isEmpty()) {
            log.info("Return cached users");
            return StudentTestCache.users;
        }

        log.info("Return non-cached users");
        Object methodResult = joinPoint.proceed();
        StudentTestCache.users = (List<User>) methodResult;
        return methodResult;
    }

}
