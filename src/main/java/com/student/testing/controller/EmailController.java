package com.student.testing.controller;

import com.student.testing.entity.EmailConfirmationToken;
import com.student.testing.entity.User;
import com.student.testing.entity.dto.response.GeneralResponseDTO;
import com.student.testing.entity.dto.response.UserResponseDTO;
import com.student.testing.exception.NoTokenException;
import com.student.testing.service.EmailConfirmationService;
import com.student.testing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    private final EmailConfirmationService emailConfirmationService;

    private final UserService userService;

    @Autowired
    public EmailController(EmailConfirmationService emailConfirmationService, UserService userService) {
        this.emailConfirmationService = emailConfirmationService;
        this.userService = userService;
    }

    @GetMapping(value = "/confirm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public GeneralResponseDTO<UserResponseDTO> confirmEmail(@RequestParam("token") String token) {
        EmailConfirmationToken emailConfirmationToken = emailConfirmationService
                .findByToken(token).orElseThrow(NoTokenException::new);

        User user = emailConfirmationToken.getUser();
        user.setEnabled(true);
        userService.update(emailConfirmationToken.getUser());

        emailConfirmationService.delete(emailConfirmationToken);

        return new GeneralResponseDTO<>(new UserResponseDTO(user));
    }

}
