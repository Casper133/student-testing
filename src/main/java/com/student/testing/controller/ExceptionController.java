package com.student.testing.controller;

import com.student.testing.entity.dto.response.GeneralResponseDTO;
import com.student.testing.exception.NoAnswerException;
import com.student.testing.exception.NoQuestionException;
import com.student.testing.exception.NoTokenException;
import com.student.testing.exception.NoUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

@RestControllerAdvice
public class ExceptionController {

    private final MessageSource messageSource;

    @Autowired
    public ExceptionController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(NoUserException.class)
    public ResponseEntity<GeneralResponseDTO> handleNoUserException(Locale locale) {
        return getResponseEntity("userNotExistMsg", locale);
    }

    @ExceptionHandler(NoQuestionException.class)
    public ResponseEntity<GeneralResponseDTO> handleNoQuestionException(Locale locale) {
        return getResponseEntity("questionNotExistMsg", locale);
    }

    @ExceptionHandler(NoAnswerException.class)
    public ResponseEntity<GeneralResponseDTO> handleNoAnswerException(Locale locale) {
        return getResponseEntity("answerNotExistMsg", locale);
    }

    @ExceptionHandler(NoTokenException.class)
    public ResponseEntity<GeneralResponseDTO> handleNoTokenException(Locale locale) {
        return getResponseEntity("tokenNotExistMsg", locale);
    }

    private ResponseEntity<GeneralResponseDTO> getResponseEntity(String errorCode, Locale locale) {
        String errorMessage = messageSource.getMessage(errorCode, null, locale);
        GeneralResponseDTO responseDTO =
                new GeneralResponseDTO(HttpStatus.NOT_FOUND.value(), errorMessage);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseDTO);
    }

}
