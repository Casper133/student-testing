package com.student.testing.controller;

import com.student.testing.entity.Question;
import com.student.testing.entity.dto.request.QuestionRequestDTO;
import com.student.testing.entity.dto.response.GeneralResponseDTO;
import com.student.testing.exception.NoQuestionException;
import com.student.testing.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping("/question")
    public GeneralResponseDTO<Question> createQuestion(@RequestBody QuestionRequestDTO bodyQuestion) {
        return new GeneralResponseDTO<>(questionService.save(new Question(bodyQuestion)));
    }

    @GetMapping("/question")
    public GeneralResponseDTO<List> getAllQuestions() {
        return new GeneralResponseDTO<>(questionService.findAll());
    }

    @GetMapping("/question/{question_id}")
    public GeneralResponseDTO<Question> getQuestionById(@PathVariable("question_id") long questionId) {
        return new GeneralResponseDTO<>(questionService.findById(questionId)
                .orElseThrow(NoQuestionException::new));
    }

    @GetMapping("/question/{question_id}/answer")
    public GeneralResponseDTO<List> getQuestionAnswers(@PathVariable("question_id") long questionId) {
        Question question = questionService.findById(questionId)
                .orElseThrow(NoQuestionException::new);

        return new GeneralResponseDTO<>(question.getAnswers());
    }

    @PutMapping("/question/{question_id}")
    public GeneralResponseDTO<Question> editQuestion(@PathVariable("question_id") long questionId,
                                                     @RequestBody QuestionRequestDTO bodyQuestion) {

        Question question = questionService.findById(questionId)
                .orElseThrow(NoQuestionException::new);

        String text = bodyQuestion.getText();
        if (text != null) {
            question.setText(text);
        }

        return new GeneralResponseDTO<>(question);
    }

    @DeleteMapping("/question/{question_id}")
    public GeneralResponseDTO<Question> deleteQuestion(@PathVariable("question_id") long questionId) {
        Question question = questionService.findById(questionId)
                .orElseThrow(NoQuestionException::new);

        questionService.delete(question);
        return new GeneralResponseDTO<>(question);
    }

}
