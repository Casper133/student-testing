package com.student.testing.controller;

import com.student.testing.entity.Answer;
import com.student.testing.entity.Question;
import com.student.testing.entity.dto.request.AnswerRequestDTO;
import com.student.testing.entity.dto.response.GeneralResponseDTO;
import com.student.testing.exception.NoAnswerException;
import com.student.testing.exception.NoQuestionException;
import com.student.testing.service.AnswerService;
import com.student.testing.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AnswerController {

    private final AnswerService answerService;

    private final QuestionService questionService;

    @Autowired
    public AnswerController(AnswerService answerService, QuestionService questionService) {
        this.answerService = answerService;
        this.questionService = questionService;
    }

    @PostMapping("/answer")
    public GeneralResponseDTO<Answer> createAnswer(@RequestBody AnswerRequestDTO bodyAnswer) {
        Question question = questionService.findById(bodyAnswer.getQuestionId())
                .orElseThrow(NoQuestionException::new);
        Answer answer = answerService.save(new Answer(bodyAnswer.getText(), question));

        return new GeneralResponseDTO<>(answer);
    }

    @PutMapping("/answer/{answer_id}")
    public GeneralResponseDTO<Answer> editAnswer(@PathVariable("answer_id") long answerId,
                                                 @RequestBody AnswerRequestDTO bodyAnswer) {

        Answer answer = answerService.findById(answerId).orElseThrow(NoAnswerException::new);
        answerService.editAnswerObject(answer, bodyAnswer);

        return new GeneralResponseDTO<>(answerService.update(answer));
    }

    @DeleteMapping("/answer/{answer_id}")
    public GeneralResponseDTO<Answer> deleteAnswer(@PathVariable("answer_id") long answerId) {
        Answer answer = answerService.findById(answerId).orElseThrow(NoAnswerException::new);
        answerService.delete(answer);

        return new GeneralResponseDTO<>(answer);
    }

}
