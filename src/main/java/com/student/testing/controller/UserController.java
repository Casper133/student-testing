package com.student.testing.controller;

import com.student.testing.config.property.EmailProperties;
import com.student.testing.entity.EmailConfirmationToken;
import com.student.testing.entity.User;
import com.student.testing.entity.dto.request.UserRequestDTO;
import com.student.testing.entity.dto.response.GeneralResponseDTO;
import com.student.testing.entity.dto.response.UserResponseDTO;
import com.student.testing.exception.NoUserException;
import com.student.testing.service.EmailConfirmationService;
import com.student.testing.service.EmailService;
import com.student.testing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserService userService;

    private final EmailConfirmationService emailConfirmationService;

    private final EmailService emailService;

    private final EmailProperties emailProperties;

    @Autowired
    public UserController(UserService userService, EmailConfirmationService emailConfirmationService,
                          EmailService emailService, EmailProperties emailProperties) {
        this.userService = userService;
        this.emailConfirmationService = emailConfirmationService;
        this.emailService = emailService;
        this.emailProperties = emailProperties;
    }

    @PostMapping("/user")
    public GeneralResponseDTO<UserResponseDTO> createUser(@RequestBody UserRequestDTO bodyUser) {
        User user = new User(bodyUser);
        user.setRole("ROLE_USER");
        user = userService.save(user);

        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(user);
        emailConfirmationService.save(emailConfirmationToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        String link = emailProperties.getLink() + emailConfirmationToken.getToken();

        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject(emailProperties.getSubject());
        mailMessage.setFrom(emailProperties.getFrom());
        mailMessage.setText(emailProperties.getText() + link);

        new Thread(() -> emailService.sendEmail(mailMessage)).start();

        return new GeneralResponseDTO<>(new UserResponseDTO(user));
    }

    @GetMapping("/user")
    public GeneralResponseDTO<List> getAllUsers() {
        List<User> users = userService.findAll();
        List<UserResponseDTO> dtoUsers = new ArrayList<>();
        users.forEach(user -> dtoUsers.add(new UserResponseDTO(user)));

        return new GeneralResponseDTO<>(dtoUsers);
    }

    @GetMapping("/user/{user_id}")
    public GeneralResponseDTO<UserResponseDTO> getUserById(@PathVariable("user_id") long userId) {
        User user = userService.findById(userId).orElseThrow(NoUserException::new);

        return new GeneralResponseDTO<>(new UserResponseDTO(user));
    }

    @PutMapping("/user/{user_id}")
    public GeneralResponseDTO<UserResponseDTO> editUser(@PathVariable("user_id") long userId,
                                                        @RequestBody UserRequestDTO bodyUser) {

        User user = userService.findById(userId).orElseThrow(NoUserException::new);
        userService.editUserObject(user, bodyUser);

        return new GeneralResponseDTO<>(new UserResponseDTO(userService.update(user)));
    }

    @DeleteMapping("/user/{user_id}")
    public GeneralResponseDTO<UserResponseDTO> deleteUser(@PathVariable("user_id") long userId) {
        User user = userService.findById(userId).orElseThrow(NoUserException::new);
        userService.delete(user);

        return new GeneralResponseDTO<>(new UserResponseDTO(user));
    }

}
