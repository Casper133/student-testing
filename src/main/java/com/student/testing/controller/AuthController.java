package com.student.testing.controller;

import com.student.testing.config.property.JwtProperties;
import com.student.testing.exception.NoUserException;
import com.student.testing.security.TokenInfo;
import com.student.testing.security.TokenType;
import com.student.testing.security.TokenUtils;
import com.student.testing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AuthController {

    private final JwtProperties jwtProperties;

    private final UserService userService;

    @Autowired
    public AuthController(JwtProperties jwtProperties, UserService userService) {
        this.jwtProperties = jwtProperties;
        this.userService = userService;
    }

    @GetMapping("/api/auth/token")
    public ResponseEntity refreshToken(@RequestHeader("Refresh") String refreshToken) {
        TokenInfo tokenInfo = TokenUtils.parseToken(refreshToken, jwtProperties);

        if (tokenInfo == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        String tokenType = tokenInfo.getTokenType();
        String email = tokenInfo.getEmail();

        if (!tokenType.equals(TokenType.REFRESH_TOKEN.toString()) || email == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        userService.findByEmail(email).orElseThrow(NoUserException::new);

        List<String> roles = tokenInfo.getRoles()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        String newAccessToken = TokenUtils.createToken(jwtProperties, email, roles,
                TokenType.ACCESS_TOKEN.toString(), jwtProperties.getAccessTokenExpirationTime());

        String newRefreshToken = TokenUtils.createToken(jwtProperties, email, roles,
                TokenType.REFRESH_TOKEN.toString(), jwtProperties.getRefreshTokenExpirationTime());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(jwtProperties.getAccessTokenHeader(), newAccessToken);
        responseHeaders.add(jwtProperties.getRefreshTokenHeader(), newRefreshToken);

        return ResponseEntity.ok().headers(responseHeaders).build();
    }

}
