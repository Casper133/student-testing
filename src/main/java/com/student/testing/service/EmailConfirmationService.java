package com.student.testing.service;

import com.student.testing.entity.EmailConfirmationToken;

import java.util.Optional;

public interface EmailConfirmationService {

    EmailConfirmationToken save(EmailConfirmationToken emailConfirmationToken);

    void delete(EmailConfirmationToken emailConfirmationToken);

    Optional<EmailConfirmationToken> findByToken(String token);

}
