package com.student.testing.service;

import com.student.testing.entity.User;
import com.student.testing.entity.dto.request.UserRequestDTO;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(User user);

    List<User> findAll();

    Optional<User> findById(long id);

    Optional<User> findByEmail(String email);

    User update(User user);

    void delete(User user);

    void editUserObject(User user, UserRequestDTO editRequestBody);

}
