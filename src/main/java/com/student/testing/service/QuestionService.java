package com.student.testing.service;

import com.student.testing.entity.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionService {

    Question save(Question question);

    List<Question> findAll();

    Optional<Question> findById(long id);

    Question update(Question question);

    void delete(Question question);

}
