package com.student.testing.service;

import com.student.testing.entity.Answer;
import com.student.testing.entity.dto.request.AnswerRequestDTO;
import com.student.testing.exception.NoQuestionException;

import java.util.List;
import java.util.Optional;

public interface AnswerService {

    Answer save(Answer answer);

    List<Answer> findAll();

    Optional<Answer> findById(long id);

    Answer update(Answer answer);

    void delete(Answer answer);

    void editAnswerObject(Answer answer, AnswerRequestDTO editRequestBody) throws NoQuestionException;

}
