package com.student.testing.service.impl;

import com.student.testing.entity.Question;
import com.student.testing.repository.QuestionRepository;
import com.student.testing.service.QuestionService;
import com.student.testing.service.StudentTestCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question save(Question question) {
        StudentTestCache.questions.clear();
        return questionRepository.save(question);
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findByOrderById();
    }

    @Override
    public Optional<Question> findById(long id) {
        return questionRepository.findById(id);
    }

    @Override
    public Question update(Question question) {
        StudentTestCache.questions.clear();
        return questionRepository.save(question);
    }

    @Override
    public void delete(Question question) {
        StudentTestCache.questions.clear();
        questionRepository.delete(question);
    }

}
