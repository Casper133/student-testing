package com.student.testing.service.impl;

import com.student.testing.entity.User;
import com.student.testing.entity.dto.request.UserRequestDTO;
import com.student.testing.repository.UserRepository;
import com.student.testing.service.StudentTestCache;
import com.student.testing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User save(User user) {
        StudentTestCache.users.clear();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getEmail().toLowerCase());
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findByOrderById();
    }

    @Override
    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User update(User user) {
        StudentTestCache.users.clear();
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        StudentTestCache.users.clear();
        userRepository.delete(user);
    }

    @Override
    public void editUserObject(User user, UserRequestDTO editRequestBody) {
        String firstName = editRequestBody.getFirstName();
        String lastName = editRequestBody.getLastName();
        String email = editRequestBody.getEmail();
        String password = editRequestBody.getPassword();

        if (isNotEmpty(firstName)) {
            user.setFirstName(firstName);
        }

        if (isNotEmpty(lastName)) {
            user.setLastName(lastName);
        }

        if (isNotEmpty(email)) {
            user.setEmail(email.toLowerCase());
        }

        if (isNotEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password));
        }
    }

    private boolean isNotEmpty(String value) {
        return value != null && value.trim().length() > 0;
    }

}
