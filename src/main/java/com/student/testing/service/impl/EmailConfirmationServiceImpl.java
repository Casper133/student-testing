package com.student.testing.service.impl;

import com.student.testing.entity.EmailConfirmationToken;
import com.student.testing.repository.EmailConfirmationTokenRepository;
import com.student.testing.service.EmailConfirmationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmailConfirmationServiceImpl implements EmailConfirmationService {

    private final EmailConfirmationTokenRepository emailConfirmationTokenRepository;

    @Autowired
    public EmailConfirmationServiceImpl(EmailConfirmationTokenRepository emailConfirmationTokenRepository) {
        this.emailConfirmationTokenRepository = emailConfirmationTokenRepository;
    }

    @Override
    public EmailConfirmationToken save(EmailConfirmationToken emailConfirmationToken) {
        return emailConfirmationTokenRepository.save(emailConfirmationToken);
    }

    @Override
    public void delete(EmailConfirmationToken emailConfirmationToken) {
        emailConfirmationTokenRepository.delete(emailConfirmationToken);
    }

    @Override
    public Optional<EmailConfirmationToken> findByToken(String token) {
        return emailConfirmationTokenRepository.findByToken(token);
    }

}
