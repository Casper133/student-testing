package com.student.testing.service.impl;

import com.student.testing.entity.Answer;
import com.student.testing.entity.Question;
import com.student.testing.entity.dto.request.AnswerRequestDTO;
import com.student.testing.exception.NoQuestionException;
import com.student.testing.repository.AnswerRepository;
import com.student.testing.service.AnswerService;
import com.student.testing.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;

    private final QuestionService questionService;

    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository, QuestionService questionService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
    }

    @Override
    public Answer save(Answer answer) {
        return answerRepository.save(answer);
    }

    @Override
    public List<Answer> findAll() {
        return answerRepository.findByOrderById();
    }

    @Override
    public Optional<Answer> findById(long id) {
        return answerRepository.findById(id);
    }

    @Override
    public Answer update(Answer answer) {
        return answerRepository.save(answer);
    }

    @Override
    public void delete(Answer answer) {
        answerRepository.delete(answer);
    }

    @Override
    public void editAnswerObject(Answer answer, AnswerRequestDTO editRequestBody) throws NoQuestionException {
        Long questionId = editRequestBody.getQuestionId();
        if (Objects.nonNull(questionId)) {
            Question question = questionService.findById(questionId).orElseThrow(NoQuestionException::new);
            answer.setQuestion(question);
        }

        String text = editRequestBody.getText();
        if (Objects.nonNull(text)) {
            answer.setText(text);
        }
    }

}
