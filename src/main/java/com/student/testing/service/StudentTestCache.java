package com.student.testing.service;

import com.student.testing.entity.Question;
import com.student.testing.entity.User;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class StudentTestCache {

    public static List<Question> questions = new CopyOnWriteArrayList<>();

    public static List<User> users = new CopyOnWriteArrayList<>();

}
