package com.student.testing.repository;

import com.student.testing.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AnswerRepository extends JpaRepository<Answer, Long> {

    List<Answer> findByOrderById();

    Optional<Answer> findById(long id);

}
