package com.student.testing.unit;

import com.student.testing.entity.User;
import com.student.testing.repository.UserRepository;
import com.student.testing.service.UserService;
import com.student.testing.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    @Mock
    private UserRepository userRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private UserService userService;

    @Before
    public void initService() {
        userService = new UserServiceImpl(userRepository, passwordEncoder);
    }

    @Test
    public void saveTest_1() {
        String firstName = "Qwerty";
        String lastName = "Test";
        String email = "qwerty@gmail.com";
        String password = "qwertyuiop";

        User user = new User(firstName, lastName, email, password);

        Mockito.when(userRepository.save(user))
                .then(AdditionalAnswers.returnsFirstArg());
        user = userService.save(user);
        Mockito.verify(userRepository).save(Mockito.any());

        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(email, user.getEmail());
        Assert.assertTrue(passwordEncoder.matches(password, user.getPassword()));
    }

}
