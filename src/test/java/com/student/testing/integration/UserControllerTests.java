package com.student.testing.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.student.testing.controller.AnswerController;
import com.student.testing.controller.QuestionController;
import com.student.testing.entity.User;
import com.student.testing.entity.dto.request.UserRequestDTO;
import com.student.testing.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class UserControllerTests {

    @MockBean
    private UserService userService;

    @MockBean
    private AnswerController answerController;

    @MockBean
    private QuestionController questionController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createUserTest() throws Exception {
        User user = new User();
        Mockito.when(userService.save(user)).thenReturn(user);
        UserRequestDTO requestDTO = new UserRequestDTO();
        ObjectMapper objectMapper = new ObjectMapper();
        String body = objectMapper.writeValueAsString(requestDTO);

        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isOk());

        Mockito.verify(userService).save(Mockito.any());
    }

}
