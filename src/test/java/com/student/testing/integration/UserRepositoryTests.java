package com.student.testing.integration;

import com.student.testing.entity.User;
import com.student.testing.repository.UserRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {UserRepositoryTests.Initializer.class})
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    private String testFirstName = "Qwerty";
    private String testLastName = "Asd";
    private String testEmail = "test@gmail.com";
    private String testPassword = "TestPass";

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer =
            new PostgreSQLContainer("postgres:12")
                    .withDatabaseName("student_tests")
                    .withUsername("student")
                    .withPassword("Qwerty");

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(applicationContext.getEnvironment());
        }
    }

    @Test
    public void saveUser_correctFieldsAfterSavingToDatabase() {
        saveUser();
        List<User> users = userRepository.findByOrderById();
        assertEquals(1, users.size());

        User user = users.get(0);
        assertEquals(testFirstName, user.getFirstName());
        assertEquals(testLastName, user.getLastName());
        assertEquals(testEmail, user.getEmail());
        assertEquals(testPassword, user.getPassword());
    }

    @Test
    public void deleteSavedUser_noUsersInDatabase() {
        User user = saveUser();

        userRepository.delete(user);
        userRepository.flush();

        List<User> users = userRepository.findByOrderById();
        assertEquals(0, users.size());
    }

    private User saveUser() {
        User user = userRepository.save(new User(testFirstName, testLastName, testEmail, testPassword));
        userRepository.flush();

        return user;
    }

}
