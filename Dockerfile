FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD build/libs/student-testing.jar student-testing.jar
EXPOSE 8070
ENTRYPOINT ["java", "-jar", "student-testing.jar"]
